package com.givewaygames.nightlight;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;

public class LightActivity extends Activity implements OnClickListener {
	
	View background;
	
	float percent = 1.0f;

    @TargetApi(11)
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        
        setContentView(R.layout.activity_light);
        
        background = findViewById(R.id.background);
        background.setOnClickListener(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
        	background.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
        }
        
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
		percent = settings.getFloat("brightness", 1.0f);
		
		updateBackground(true);
    }
    
    @Override
    protected void onPause() {
    	super.onPause();
    	
    	SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
		settings.edit().putFloat("brightness", percent).commit();
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
    	updateBackground(true);
    }
    
    private void updateBackground(boolean readValue) {
    	if (readValue) {
        	SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
    		percent = settings.getFloat("brightness", 1.0f);
    	}
		
		int value = (int)(percent * 255);
		background.setBackgroundColor(0xFF000000 | value | value << 8 | value << 16);
		
		
		WindowManager.LayoutParams lp = getWindow().getAttributes();
		lp.screenBrightness = percent;
		getWindow().setAttributes(lp);
    }

	@Override
	public void onClick(View v) {
		if (percent >= 1.0f) {
			percent = 0.2f;
		} else {
			percent += 0.2f;
		}
		
		updateBackground(false);
	}
    
    
}
